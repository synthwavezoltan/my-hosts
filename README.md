# My hosts blacklist

My personal crusade for a healthier internet. Highly subjective.

## About

`hosts` contains the hosts usage I personally want to use. I made it public so that you can find usefulness in it.

`your.hosts` contains all the records in `hosts` plus the ones I personally don't want to filter, but others may want to.

This list is curated *by hand* (with the exception of background sources; see below) - since I'm not using on servers, I wouldn't prefer setting up an update infrastructure like the great [Ultimate Hosts Blacklists](https://github.com/Ultimate-Hosts-Blacklist/Ultimate.Hosts.Blacklist) does that. 

As such, this list is not intended to filter *absolutely everything*, just sort of a rule-of-thumb. Of course, as time goes on, and I find domains I can easily find and open (e.g. tiktok.com vs vm.tiktok.com), I'm going to add those extras, but I'm not overworking at first.

The primary goal here is to filter out sites I'm outright totally sure would hinder my productivity or well-being without the slightest of extra benefits. This includes the following categories:

* **distracting sites**: social media sites I'm not using for anything useful.
* **useless news/explicit propaganda sites**: news, or alleged "news" portals known for misinformation, or opinionated pieces.
* **background sources**: sources of ads, trackers, and other nasty stuff.
* **potentially malicious sites**: sites you may not fully trust.

Imagine this list as a sort-of filtering for when you need to work on something important.

Please note that what is a "false positive" may depend on your personal preference; this is a totally subjective list. Naturally, you can use it by removing anything you'd actually need to use.


## How to use

On Linux, replace `/etc/hosts` file with the desired one from this repo.

## Contribute

If you think you want to, open a merge request. Please modify `your.hosts` only!

## Credits

Lots of various repos that helped me:

* <https://github.com/belavizer/hungarian.hosts.file>
* <https://github.com/Caticer/block-tiktok>
* <https://github.com/Ewpratten/youtube_ad_blocklist>
* <https://github.com/StevenBlack/hosts>
* <https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt>